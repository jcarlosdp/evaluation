package mx.com.payclip.evaluation.business.test;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import mx.com.payclip.evaluation.AppConfig;
import mx.com.payclip.evaluation.bean.UserTransaction;
import mx.com.payclip.evaluation.business.UserTransactionRepository;
import mx.com.payclip.evaluation.util.JsonManager;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = AppConfig.class)
public class UserTransactionRepositoryTest {

	@Autowired
	private UserTransactionRepository userRepository;

	@Autowired
	private JsonManager jsonManager;

	@Test
	public void testSaveTransaction() {

		final String jsonString = "{\n" + "	\"amount\": 1.23,\n" + "	\"description\": \"Joes Tacos\",\n"
				+ "	\"date\": \"2018-12-30\",\n" + "	\"user_id\": 346\n" + "}";

		final UserTransaction userTransaction = jsonManager.jsonToObject(jsonString, UserTransaction.class);
		userRepository.saveTransaction(346L, userTransaction);
	}

	@Test
	public void testFindByUserIdAndTransactionId() {

		assertNotNull(userRepository.findByUserIdAndTransactionId(346L, "83261345-e1fe-4232-9a75-625a0f29fa37"));
	}

	@Test
	public void testFindAllByUserId() {

		assertNotNull(userRepository.findAllByUserId(346L));
	}

	@Test
	public void testGetSumByUserId() {

		assertNotNull(userRepository.getSumByUserId(346L));
	}

}
