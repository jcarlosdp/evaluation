package mx.com.payclip.evaluation.bean;

import java.util.Date;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

public class UserTransaction {

	private Double amount;
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date date;
	private String description;
	@JsonProperty("transaction_id")
	private String transactionId;
	@JsonProperty("user_id")
	private Long userId;

	private UserTransaction() {

		transactionId = UUID.randomUUID().toString();
	}

	public UserTransaction(final Double amount, final Date date, final String description, final Long userId) {
		this();
		this.amount = amount;
		this.date = date;
		this.description = description;
		this.userId = userId;
	}

	public Double getAmount() {
		return amount;
	}

	public Date getDate() {
		return date;
	}

	public String getDescription() {
		return description;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public Long getUserId() {
		return userId;
	}

	
	
	
}
