package mx.com.payclip.evaluation.util;

import java.util.Arrays;
import java.util.Optional;

public enum TransactionType {

	ADD_TRANSACTION(1), SHOW_TRANSACTION(2), LIST_TRANSACTIONS(3), SUM_TRANSACTION(4);

	private final int transaction;

	private TransactionType(final int transaction) {
		this.transaction = transaction;

	}

	public int getTransaction() {
		return transaction;
	}

	public static Optional<TransactionType> valueOf(final int value) {
		return Arrays.stream(values()).filter(option -> option.getTransaction() == value).findFirst();
	}

}
