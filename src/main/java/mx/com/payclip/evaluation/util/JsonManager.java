package mx.com.payclip.evaluation.util;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class JsonManager {

	private static final Logger LOG = LoggerFactory.getLogger(JsonManager.class);

	@Autowired
	private ObjectMapper objectMapper;

	public <T> T jsonToObject(final String jsonChain, final Class<T> clazz) {

		T object = null;

		if (isValidJSON(jsonChain)) {

			try {
				object = objectMapper.readValue(jsonChain, clazz);

			} catch (IOException e) {
				LOG.error("Error to read JSON, cause {}", e);
			}
		}
		return object;

	}

	public <T> void printResult(final T result) {

		try {

			LOG.info("Result\n" + objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(result));

		} catch (JsonProcessingException e) {
			LOG.error("Error to process JSON String", e);
		}

	}

	private boolean isValidJSON(final String json) {
		boolean valid = true;
		try {
			objectMapper.readTree(json);
		} catch (JsonProcessingException e) {
			valid = false;
		}
		return valid;
	}

}
