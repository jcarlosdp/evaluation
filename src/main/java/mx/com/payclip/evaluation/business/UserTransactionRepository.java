package mx.com.payclip.evaluation.business;

import java.util.List;
import java.util.Optional;

import mx.com.payclip.evaluation.bean.UserTransaction;

public interface UserTransactionRepository {

	Optional<UserTransaction> findByUserIdAndTransactionId(Long userId, String transactionId);

	List<UserTransaction> findAllByUserId(Long userId);

	void saveTransaction(Long userId, UserTransaction userTransaction);

	Double getSumByUserId(Long userId);

}