package mx.com.payclip.evaluation.business.impl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import mx.com.payclip.evaluation.bean.UserTransaction;
import mx.com.payclip.evaluation.business.UserTransactionRepository;
import mx.com.payclip.evaluation.util.JsonManager;

@Component
public class UserTransactionRepositoryImpl implements UserTransactionRepository {

	private static final String RELATIVE_PATH = "./userTransactions/";
	private static final Logger LOG = LoggerFactory.getLogger(UserTransactionRepositoryImpl.class);

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private JsonManager jsonManager;

	public Optional<UserTransaction> findByUserIdAndTransactionId(final Long userId, final String transactionId) {

		Optional<UserTransaction> userOptional = null;

		final List<UserTransaction> users = getUserTransactions(userId);

		if (users.isEmpty()) {
			LOG.info("Transaction not found");
		} else {

			userOptional = users.stream().filter(userTrans -> userTrans.getUserId().equals(userId)
					&& userTrans.getTransactionId().equals(transactionId)).findFirst();

			if (userOptional.isPresent()) {

				jsonManager.printResult(userOptional.get());
			} else {
				LOG.info("Transaction not found");

			}

		}
		return userOptional;

	}

	public List<UserTransaction> findAllByUserId(final Long userId) {

		final List<UserTransaction> transactions = getUserTransactions(userId);

		if (!transactions.isEmpty()) {
			jsonManager.printResult(transactions);
		} else {
			LOG.info("Transactions not found");
		}

		return transactions;
	}

	public void saveTransaction(final Long userId, final UserTransaction userTransaction) {

		final List<UserTransaction> users = getUserTransactions(userId);
		users.add(userTransaction);
		try {
			final ObjectWriter writer = objectMapper.writer(new DefaultPrettyPrinter());
			writer.writeValue(getFile(userTransaction.getUserId()), users);
			LOG.info("\n##############Transaction saved successfully##################");
		} catch (IOException e) {
			LOG.error("Error to write file, cause {}", e);
		}

	}

	public Double getSumByUserId(final Long userId) {

		Double sum = 0.0;
		final List<UserTransaction> transactions = getUserTransactions(userId);

		if (!transactions.isEmpty()) {
			sum = transactions.stream().mapToDouble(userTrans -> userTrans.getAmount()).sum();

			LOG.info("The Total is: " + sum);

		} else {
			LOG.info("Transactions not found");
		}
		return sum;

	}

	private File getFile(final Long userId) {

		return new File(RELATIVE_PATH + userId + ".json");
	}

	private List<UserTransaction> getUserTransactions(final Long userId) {

		final File file = getFile(userId);
		List<UserTransaction> userTransactions = null;
		if (file.exists()) {

			try {
				userTransactions = objectMapper.readValue(file, new TypeReference<List<UserTransaction>>() {
				});

			} catch (IOException e) {
				LOG.error("Error to write file, cause {}", e);
			}

		} else {

			userTransactions = new ArrayList<>();
		}
		return userTransactions;
	}

}
