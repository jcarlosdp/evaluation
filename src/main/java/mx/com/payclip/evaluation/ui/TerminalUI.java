package mx.com.payclip.evaluation.ui;

import java.util.function.Consumer;

import org.beryx.textio.TextIO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import mx.com.payclip.evaluation.bean.UserTransaction;
import mx.com.payclip.evaluation.business.UserTransactionRepository;
import mx.com.payclip.evaluation.util.JsonManager;
import mx.com.payclip.evaluation.util.TransactionType;

@Component
public class TerminalUI implements Consumer<TextIO> {

	@Autowired
	private JsonManager jsonManager;

	@Autowired
	private UserTransactionRepository userRepository;

	private static final Logger LOG = LoggerFactory.getLogger(TerminalUI.class);
	private static final String INPUT_USER_ID = "Please enter user identifier [user_id]:";

	@Override
	public void accept(TextIO textIO) {

		final TransactionType typeTransaction = textIO.newEnumInputReader(TransactionType.class)
				.read("\nWhat Transaction do you want perform?:");

		final Long userId = textIO.newLongInputReader().read(INPUT_USER_ID);

		switch (typeTransaction) {

		case ADD_TRANSACTION:

			final String jsonString = textIO.newStringInputReader().read("Please enter transaction in JSON format: ");

			final UserTransaction userTransaction = jsonManager.jsonToObject(jsonString, UserTransaction.class);

			if (userTransaction == null) {
				textIO.newStringInputReader().withMinLength(0)
						.read("\n##########Invalid JSON please, verify your data #######\n");

			} else {
				userRepository.saveTransaction(userId, userTransaction);
			}

			break;
		case SHOW_TRANSACTION:

			final String transactionId = textIO.newStringInputReader()
					.read("Please enter transaction identifier [transaction_id]:");

			userRepository.findByUserIdAndTransactionId(userId, transactionId);
			break;
		case LIST_TRANSACTIONS:

			userRepository.findAllByUserId(userId);
			break;
		case SUM_TRANSACTION:
			userRepository.getSumByUserId(userId);
			break;

		default:
			LOG.error("Unsupported transaction type");
			break;
		}

		textIO.dispose("Program finished.");

	}

}
