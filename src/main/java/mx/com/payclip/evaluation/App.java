package mx.com.payclip.evaluation;

import org.beryx.textio.TextIO;
import org.beryx.textio.system.SystemTextTerminal;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import mx.com.payclip.evaluation.ui.TerminalUI;

public class App {

	public static void main(final String[] args) {

		final ApplicationContext context = new AnnotationConfigApplicationContext(new Class[] { AppConfig.class });
		final TerminalUI terminal = context.getBean(TerminalUI.class);
		final SystemTextTerminal xterm = new SystemTextTerminal();
		final TextIO textIO = new TextIO(xterm);

		terminal.accept(textIO);

		((ConfigurableApplicationContext) context).close();

	}

}
