# Payclip Evaluation

Hello Mr. Kevin Davies, please follow the bellow instruction to setup the application.

## 1. Clone a repository

Use these steps to clone from **Bitbucket Repository**

1. Please validate your access to [the Git Repository](https://jcarlosdp@bitbucket.org/jcarlosdp/evaluation.git).
2. Clone the project to your local machine 

## 2. Compile the application

Execute the bellow command under **evaluation** project folder to generate the jar file:

```
mvn clean install
```

## 3. Execute the application
Please open your prefer terminal an go into the folder:  cd (absolute path) **\evaluation\workDirectory**

a) For Windows execute application.cmd:

```
.\application.cmd
```

b) For Linux execution use the bellow shell:

```
sh application.sh
```

## 4. Command line interface
Please take a look in the bellow options and test as you want:

```
What Transaction do you want perform?:
  1: ADD_TRANSACTION
  2: SHOW_TRANSACTION
  3: LIST_TRANSACTIONS
  4: SUM_TRANSACTION
Enter your choice: 
```

**For ADD_TRANSACTION**, please enter the JSON chain in one line, as follow:

```
 {"amount" : 1.23, "date" : "2018-12-30", "description" : "Joes Tacos","transaction_id" : "83261345-e1fe-4232-9a75-625a0f29fa37", "user_id" : 346}
```

## 5. Validate the JSON File
If you want to see the JSON files generated please open the folder cd(absolute path) **\evaluation\workDirectory\userTransactions**, the solution was generate one json file per userId




As you can see, the batch processes: application.cmd and apllication.sh are really simple but for the development of the command line interface I use [Text.io](https://text-io.beryx.org/releases/latest/).

Thank you for let me participate in the hiring!!!

Developer: Juan Carlos Duarte Pérez
Mail: jcarlosdp12@gmail.com
