#!/bin/sh

###################################################################
#Script Name	: Payclip Evaluation                                                                                              
#Description	: Solution to the evaluation with org.beryx-text-io                                                                                
#Args           	:  N/A                                                                                         
#Author       	:Juan Carlos Duarte Perez                                               
#Email         	:jcarlosdp12@gmail.com                                      
###################################################################

java -jar evaluation.jar