REM ###################################################################
REM #Script Name	: Payclip Evaluation                                                                                              
REM #Description	: Solution to the evaluation with org.beryx-text-io                                                                                
REM #Args           	:  N/A                                                                                         
REM #Author       	:Juan Carlos Duarte Perez                                               
REM #Email         	:jcarlosdp12@gmail.com                                      
REM ###################################################################

@ECHO OFF
start java -jar evaluation.jar